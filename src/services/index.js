import {
    Create, Delete, FindOne, IfExist, Update,
    View, Search, IfExistsUsername,
    callCreateUser, Test
} from '../controllers';

export const HandleCreate = async ({ fields }) => {
    try {

        let { username } = fields;

        if (await IfExistsUsername({ username })) {
            return { statusCode: 400, message: "Ya existe un usuario registrado bajo ese nombre" }
        }

        else {

            let { statusCode, data, message } = await Create({ fields });

            callCreateUser(fields)
            callNotificationsOverPhone({ phone: fields.phone })

            return { statusCode, data, message };
        }

    } catch (error) {

    }
};

export const HandleUpdate = async ({ pk, fields }) => {
    try {

    } catch (error) {

    }
};

export const HandleDelete = async ({ pk }) => {
    try {

    } catch (error) {

    }
};

export const HandleSearchByPhone = async ({ limit, offset, phone, exclude }) => {
    try {

        let result = await Test();

        if (result.statusCode === 200 && result.data === true) {

            let { statusCode, data, message } = await Search({
                limit,
                offset,
                search: phone,
                exclude
            });

            return { statusCode, data, message };
        }

        else return {
            statusCode: 400,
            message: "No podes realizar este tipo de busqueda por que lo digo yo"
        };

    } catch (error) {
        console.log({ step: "service handleViewPhone", error: error.toString() })
        return { statusCode: 500, message: "No podemos procesar tu solicitud intenta más tarde" }
    }
};

export const HandleViewRegisterUser = async ({ limit, offset, registerUser }) => {
    try {

        let { statusCode, data, message } = await View({
            limit,
            offset,
            where: { registerUser }
        });

        return { statusCode, data, message };

    } catch (error) {
        console.log({ step: "service handleViewRegisterUser", error: error.toString() })
        return { statusCode: 500, message: "No podemos procesar tu solicitud intenta más tarde" }
    }
};

export const HandleView = async ({ limit, offset }) => {
    try {

        let { statusCode, data, message } = await View({ limit, offset });

        return { statusCode, data, message };

    } catch (error) {
        console.log({ step: "service handleView", error: error.toString() })
        return { statusCode: 500, message: "No podemos procesar tu solicitud intenta más tarde" }
    }
};

export const HandleFindOne = async ({ pk }) => {
    try {

    } catch (error) {

    }
};

export const HandleIfExists = async ({ pk }) => {
    try {

    } catch (error) {

    }
};
