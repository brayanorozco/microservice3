//ECMA SCRIPT 5
const Bull = require('bull');
const dotenv = require('dotenv');

dotenv.config();

const REDIS = {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT
}

const opts = {
    redis: {
        host: REDIS.host,
        port: REDIS.port
    }
};

const queueView = new Bull("microservice3:view", opts);
const queueUpdate = new Bull("microservice3:update", opts);
const queueDelete = new Bull("microservice3:delete", opts);
const queueCreate = new Bull("microservice3:create", opts);
const queueFindOne = new Bull("microservice3:findOne", opts);
const queueIfExists = new Bull("microservice3:ifExists", opts);
const queueViewRegisterUser = new Bull("microservice3:registerUser", opts);
const queueSearchByPhone = new Bull("microservice3:searchByPhone", opts);

async function main(){

        try {
            console.log("Just a test");
            
            const job = await queueView.add({ limit: 12, offset: 0 });

            const result = await job.finished();

        } catch (error) {
            console.error(error);
        }
}

main();
