import {
    DataTypes
} from "sequelize";

import { Sequelize } from 'sequelize';

import { DATABASES } from '../settings';

const sequelize = new Sequelize(
    DATABASES.NAME, DATABASES.USER, DATABASES.PASSWORD, {
    host: DATABASES.HOST,
    port: DATABASES.PORT,
    dialect: DATABASES.ENGINE
})

//esto no va aca!! revicsa los videos y el repo
//tanpoco tenes la carpeta de test !!!


export const Model = sequelize.define('model', {

    pk: {
        type: DataTypes.UUID,
        primaryKey: true,
        unique: true,
        defaultValue: DataTypes.UUDV4
    },

    phone: {
        type: DataTypes.STRING
    },

    fullname: {
        type: DataTypes.STRING
    },

    username: {
        type: DataTypes.STRING
    },

    image: {
        type: DataTypes.STRING
    },

    address: {
        type: DataTypes.STRING
    },

    zipCode: {
        type: DataTypes.STRING
    },
    email: {
        type: DataTypes.STRING
    },
    typeId: {
        type: DataTypes.BIGINT
    },
    idNumber: {
        type: DataTypes.STRING
    },
    dateOfBirth: {
        type: DataTypes.TIME
    },

    registerUser: {
        type: DataTypes.UUID
    },
    updateUser: {
        type: DataTypes.UUID
    }
}, {
    timestamps: true,
    freezeTableName: true,
});

export const syncDB = async () => {

    try {

        await Model.sync({
            logging: false,
            force: false
        });

        console.info("Model up");

    } catch (error) {
        console.info("Model down");
        console.error(error);
    }

}