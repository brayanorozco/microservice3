import {
    queueCreate,
    queueDelete,
    queueFindOne,
    queueUpdate,
    queueView,
    queueIfExists,
    queueViewRegisterUser,
    queueSearchByPhone

} from "./index";

import {
    HandleUpdate,
    HandleCreate,
    HandleView,
    HandleDelete,
    HandleFindOne,
    HandleIfExists,
    HandleViewRegisterUser,
    HandleSearchByPhone
} from "../services";

queueUpdate.process(async (job, done) => {

    try {

        let {
            pk,
            name,
            description,
            price
        } = job.data

        let {
            statusCode,
            data,
            message
        } = await HandleUpdate({
            pk,
            name,
            description,
            price
        })

        return done(null, {
            statusCode,
            data,
            message
        })

    } catch (error) {
        console.error({
            step: "adapter queueUpdate",
            error
        });

        return done(new Error({
            statusCode: 500,
            message: "Internal error server"
        }))

    }

});

queueCreate.process(async (job, done) => {

    try {

        let {
            name,
            description,
            price
        } = job.data

        let {
            statusCode,
            data,
            message
        } = await HandleCreate({
            name,
            description,
            price
        });

        return done(null, {
            statusCode,
            data,
            message
        })

    } catch (error) {
        console.error({
            step: "adapter queueCreate",
            error
        });

        return done(new Error({
            statusCode: 500,
            message: "Internal error server"
        }))

    }

});

queueView.process(async (job, done) => {

    try {

        let {
            limit,
            offset
        } = job.data

        return done(null, await HandleView({
            limit,
            offset
        }));

    } catch (error) {
        console.error({
            step: "adapter queueView",
            error
        });

        return done(new Error({
            statusCode: 500,
            message: "Internal error server"
        }))

    }

});

queueDelete.process(async (job, done) => {

    try {

        let {
            pk
        } = job.data

        let {
            statusCode,
            data,
            message
        } = await HandleDelete({
            pk
        })

        return done(null, {
            statusCode,
            data,
            message
        })

    } catch (error) {
        console.error({
            step: "adapter queueDelete",
            error
        });

        return done(new Error({
            statusCode: 500,
            message: "Internal error server"
        }))

    }

});

queueFindOne.process(async (job, done) => {

    try {

        let {
            pk
        } = job.data;

        return done(null, await HandleFindOne({
            pk
        }));

    } catch (error) {
        console.error({
            step: "adapter queueFindOne",
            error
        });

        return done(new Error({
            statusCode: 500,
            message: "Internal error server"
        }));

    }

});

queueIfExists.process(async (job, done) => {

    try {

        let {
            limit,
            offset
        } = job.data;

        return done(null, await HandleIfExists({
            pk
        }));


    } catch (error) {
        console.error({
            step: "adapter queueIfExists",
            error
        });

        return done(new Error({
            statusCode: 500,
            message: "Internal error server"
        }))

    }

});

queueSearchByPhone.process(async (job, done) => {
    try {

        let { limit, offset, phone, exclude } = job.data;

        return done(null, await HandleSearchByPhone({ limit, offset, phone, exclude }));

    } catch (error) {
        var step = "adapters queueSearchByPhone"
        console.log(step, error.toString())
        return done(new Error(step))
    }
});

queueViewRegisterUser.process(async (job, done) => {
    try {

        let { limit, offset, registerUser } = job.data;

        return done(null, await HandleViewRegisterUser({ limit, offset, registerUser }));

    } catch (error) {
        var step = "adapters queueViewRegisterUser"
        console.log(step, error.toString())
        return done(new Error(step))
    }
});

