import Bull from 'bull';

import {REDIS} from '../settings';

const opts = {
    redis: {
        host: REDIS.host,
        port: REDIS.port
    }
};


//Internals

export const queueView = new Bull("microservice3:view", opts);
export const queueUpdate = new Bull("microservice3:update", opts);
export const queueDelete = new Bull("microservice3:delete", opts);
export const queueCreate = new Bull("microservice3:create", opts);
export const queueFindOne = new Bull("microservice3:findOne", opts);
export const queueIfExists = new Bull("microservice3:ifExist", opts);
export const queueViewRegisterUser = new Bull("microservice3:registerUser", opts);
export const queueSearchByPhone = new Bull("microservice3:searchByPhone", opts);
